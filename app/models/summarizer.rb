
require 'sysinfo'

class Summarizer
  def initialize
    @sysinfo = SysInfo.new
  end

	def platform
		@sysinfo.platform
	end

end
